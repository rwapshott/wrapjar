TEMP=`mktemp badgerXXXXXX`
SOURCE_PATH=$TEMP.java
SOURCE_DIR=`dirname $SOURCE_PATH`
CLASS_PATH=$TEMP.class
CLASS_NAME=`basename $TEMP`
mv $TEMP $SOURCE_PATH

echo "public class $CLASS_NAME {" > $SOURCE_PATH
echo "public static void wrapJar(String[] args) {" >> $SOURCE_PATH
echo "/** javadoc **/" >> $SOURCE_PATH
echo "System.out.println(\"Hello World \" + args[0]);" >> $SOURCE_PATH
echo "}" >> $SOURCE_PATH
echo "}" >> $SOURCE_PATH
javac -d $SOURCE_DIR $SOURCE_PATH

java -cp . $CLASS_NAME $0