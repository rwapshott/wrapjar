Executable Wrapper
------------------

Java applications have always struggled to be first class citizens in the computer command line world.
This is for a number of reasons:

- The java command line utility requires a number of arguments all of which are unwieldy to type and
depend on locations of jars to be known to the user.

- Wrapping it in a script is little better as this defers the problem to the script to solve and maintain.

- Creating an executable jar is better, and Mavens Shade plugin or One Jar will resolve the dependencies,
however this is not PATH compatible.

After effort to resolve the basic problem, we still do not have a satisfactory solution. These issues
marginalise Java applications to enterprise only environments. Shell scripts to launch and other vagaries
which reduce the scope and usability of JVM based applications.

Executable Wrapping is present from a number of good libraries out there. Why not use those?

Sure, Launch4J for example is fine but all too often I come back to using it after a while only to
find it breaks under some new circumstance or situation. This has happened a few times and is getting
tedious.

So what is proposed solution?

One that has struck me as neat is the embedding binary data within a shell script. This strikes me as neat
because there is a shell script interpreter on every system (Windows, Linux, Mac etc). If we can devise a
system that uses that to embed binary data, then we have a solution.

Introduction
------------

This solution is perhaps a little overkill, but it works.

We take a simple Java application that is encoded into echo statements contained within the shell script.
This script will produce this source code, which we then compile and execute with the script as the parameter.
The application will find the binary data contained within the script and extract it, executing the Jar
contained with any arguments passed to the original script.

See, I told you it was a little overkill...

Dependencies
------------

This approach requires a JDK (javac will be executed) on the target system. A basic shell script interpreter
will also be required.

Example
-------

exewrapper <jar> [<jar>] [mainClass]

Each Jar file will be read in order, and the first Main-Class entry found in the manifest will be
selected to be the Main-Class for the target jar file. Unless a non file path is provided on the
command line in which case, that will be used instead.

All jars will be read in and combined together into a single jar, with the selected Main-Class which will
be used to launch the Jar.

Note: Any information held in the manifests of each Jar provided will be lost.