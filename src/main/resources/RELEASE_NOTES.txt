0.1 Initial Version, with support for Linux based shell scripts.
0.2 Cleaned up Bootstrapper output, less verbose.
0.3 fixing a bug with the output of the Bootstrap.
0.4 Added ability to override main class.
0.5 Found argument passing to be a mess, decided to re-write to a simpler solution using echo -n -e