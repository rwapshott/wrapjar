/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package uk.co.gencoreoperative.executable.script;

import com.google.inject.Inject;
import uk.co.gencoreoperative.executable.ExecutableWrapperConstants;
import uk.co.gencoreoperative.executable.Tracer;
import uk.co.gencoreoperative.fileutils.StreamUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

/**
 * Orchestrates the writing of a shell script that both contains the embedded Jar file and the commands
 * to create a bootstrap application which will extract the jar and launch it.
 *
 * Note: The ScriptWriter will generate an operating specific script.
 *
 * Note: The binary data will be encoded as base64 and gzipped to keep the size as small as possible.
 *
 * @author Robert Wapshott
 */
public class ScriptWriter {
    public static final String LINE_SEP = System.getProperty("line.separator");
    public static final String BOOTSTRAP_JAVA = ExecutableWrapperConstants.BOOTSTRAP_JAVA;
    private Script script;
    private Tracer tracer;

    @Inject
    public ScriptWriter(Script script, Tracer tracer) {
        this.script = script;
        this.tracer = tracer;
    }

    /**
     * Write the script for the given Jar file.
     *
     * @param jar Non null Jar file to encode and store.
     *
     * @param output Non null stream to write to.
     */
    public void write(File jar, OutputStream output) {
        tracer.info("Writing script contents...");
        writeScript(output);

        // The signal separates the script from the data
        writeLine(ExecutableWrapperConstants.SIGNAL, output);

        tracer.info("Writing script binary...");
        writeBinary(jar, output);

        // Finally flush to ensure all is written to the stream.
        try {
            output.flush();
        } catch (IOException e) {
            throw tracer.error(e);
        }
    }

    /**
     * Simple utility function for writing a line of text to the stream.
     *
     * @param line Line of text to write.
     *
     * @param outputStream Non null and open stream to write to.
     */
    private void writeLine(String line, OutputStream outputStream) {
        try {
            String r = line + LINE_SEP;
            outputStream.write(r.getBytes());
        } catch (IOException e) {
            throw tracer.error(e);
        }
    }

    /**
     * Write the script which contains both the commands to create the Bootstrapper application and the embedded
     * Jar file to the output stream.
     *
     * @param output A non null output stream ready to be written to.
     */
    private void writeScript(OutputStream output) {
        // Ensure the script file exists and is empty
        writeLine(script.createFile(BOOTSTRAP_JAVA), output);

        // Next, write out the Bootstrapper code with the appropriate values provided.
        InputStream stream = ScriptWriter.class.getResourceAsStream("/" + BOOTSTRAP_JAVA);
        for (String line : StreamUtils.readLinesToList(stream)) {
            writeLine(script.echoLine(line, BOOTSTRAP_JAVA), output);
        }

        // Write out the command to compile the script
        String cmd = MessageFormat.format("javac {0}", BOOTSTRAP_JAVA);
        writeLine(script.executeCommand(cmd), output);

        // Now launch the Bootstrapper class
        String launch = MessageFormat.format(
                "java -cp . {0} {1} {2}",
                ExecutableWrapperConstants.BOOTSTRAP_CLASSNAME,
                script.self(),
                script.allArguments());
        writeLine(script.executeCommand(launch), output);

        // Delete the created source and class file.
        writeLine(script.deleteFile(BOOTSTRAP_JAVA), output);
        writeLine(script.deleteFile(ExecutableWrapperConstants.BOOTSTRAP_WILDCARDED_CLASS), output);

        // Importantly trigger the script to exit
        writeLine(script.exit(), output);
    }

    /**
     * Write the binary data to the script.
     *
     * Note: This function assumes that the OutputStream has already had the script text written to it and is
     * in the correct position to now accept the binary data.
     *
     * @param jar Jar file to write.
     * @param output Stream to write to.
     */
    private void writeBinary(File jar, OutputStream output) {
        long length = jar.length();
        if (length > Integer.MAX_VALUE) {
            throw tracer.error("The file {0} is too large to encode", jar.getPath());
        }

        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            BufferedOutputStream out = new BufferedOutputStream(bout);
            InputStream in = new FileInputStream(jar);
            int read;
            while ((read = in.read()) != -1) {
                out.write(read);
            }
            out.flush();
            out.close();
            in.close();

            byte[] data = bout.toByteArray();
            String base64 = DatatypeConverter.printBase64Binary(data);
            output.write(base64.getBytes());

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw tracer.error(e);
        }
    }
}
