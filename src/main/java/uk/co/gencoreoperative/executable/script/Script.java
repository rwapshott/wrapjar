/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package uk.co.gencoreoperative.executable.script;

/**
 * The Script interface represents a sequence of callable functions which will generate operating
 * specific shell script text which can be placed into a shell script.
 *
 * Note: This interface is far from perfect and willingly leaks abstraction details in order to solve
 * this fiddly problem in a pragmatic way.
 *
 * @author Robert Wapshott
 */
public interface Script {
    /**
     * Convert the given line to an echo statement, accounting for any special characters which must
     * be escaped. The echo statement must also append to the given file location.
     *
     * @param line Possibly empty line of text to echo.
     *
     * @param file The full path to a file to append to.
     *
     * @return The complete echo and append statement.
     */
    String echoLine(String line, String file);

    /**
     * Write out the shell command to create a file and ensure that the contents of the file
     * are empty.
     *
     * @param sourcePath Non null path to the file to create.
     *
     * @return The shell script command.
     */
    String createFile(String sourcePath);

    /**
     * Delete the named file.
     *
     * @param path Non null path of the file to delete.
     *
     * @return The shell script command.
     */
    String deleteFile(String path);

    /**
     * Write the shell command to execute the given command. This approach is the same in all the target
     * shell languages so it is safe to pass in the same command for each.
     *
     * @param cmd The command to execute.
     *
     * @return The shell command.
     */
    String executeCommand(String cmd);

    /**
     * We need to ensure the script signals that it should terminate at the correct point. This will
     * allow us to embed direct binary data afterwards.
     *
     * @return A non null exit statement.
     */
    String exit();

    /**
     * Return the symbol used to refer to the script that is currently executing.
     *
     * @return A non null String.
     */
    String self();

    /**
     * @return Returns the shell script symbols for all arguments passed into the script.
     */
    String allArguments();
}
