/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package uk.co.gencoreoperative.executable.script.impl;

import uk.co.gencoreoperative.executable.script.Script;

import java.text.MessageFormat;

/**
 * Linux Bash/Sh based implementation of the Script interface.
 *
 * @author Robert Wapshott
 */
public class LinuxShell implements Script {
    public String echoLine(String line, String file) {
        String r = line.replaceAll("\"", "\\\\\"");
        return format("echo \"{0}\" >> {1}", r, file);
    }

    public String createFile(String sourcePath) {
        return format("touch {0}\n> {0}", sourcePath);
    }

    public String deleteFile(String path) {
        return format("rm -f {0}", path);
    }

    public String executeCommand(String cmd) {
        return cmd;
    }

    public String exit() {
        return "exit 0";
    }

    public String self() {
        return "$0";
    }

    public String allArguments() {
        return "\"$@\"";
    }

    private static String format(String format, String... args) {
        return MessageFormat.format(format, (Object[])args);
    }
}
