package uk.co.gencoreoperative.executable;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Reads in a Jar and outputs a self-executing script which wraps the jar.
 */
public class Main {
    @Parameter (required = false)
    private List<File> jars = new ArrayList<>();

    private static ByteArrayOutputStream bout = new ByteArrayOutputStream();
    private static PrintWriter out = new PrintWriter(new BufferedOutputStream(bout));

    public static void main(String... args) throws IOException {
        Main main = new Main();
        JCommander commander = new JCommander(main);
        commander.parse(args);

        // TODO limitation - can only support a single jar.
        List<String> targets = new ArrayList<>();

        PrintWriter script = new PrintWriter("script.sh");
        // TODO limitation - can only support a single jar.
        script.write("# Script to execute " + args[0] + "\n");

        for (File jar : main.jars) {

            String target = getRandomJarName();
            targets.add(target);

            byte[] buf = new byte[(int) jar.length()];
            FileInputStream in = new FileInputStream(jar);

            // Blank the jar to start
            // TODO - Do we need to write it if it is already there?
            script.write("> " + target + "\n");

            // Write out bytes as script.
            int read = 0;
            while (read != -1) {
                read = in.read(buf);
                if (read == -1) continue;
                script.write(bytesToShell(buf, read, target));
            }
        }

        // Write out command to start Java application
        script.write("java -jar " + targets.get(0) + " \"$@\"" +"\n");
        script.write("rm -rf " + targets.get(0) + "\n");

        script.flush();
        script.close();
    }

    private static String bytesToShell(byte[] buf, int len, String target) {
        bout.reset();
        out.print("echo -n -e \"");
        for (int ii = 0; ii < len; ii++) {
            int unsigned = 0xFF & buf[ii];
            out.print("\\x" + Integer.toHexString(unsigned));
        }
        out.println("\" >> " + target);
        out.flush();
        return new String(bout.toByteArray());
    }

    private static String getRandomJarName() {
        String random = UUID.randomUUID().toString();
        String name = random.split("-")[0] + ".jar";
        return name;
    }
}
