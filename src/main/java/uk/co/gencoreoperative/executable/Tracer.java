/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package uk.co.gencoreoperative.executable;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.text.MessageFormat;

/**
 * The simplest logging class you can get...
 *
 * @author Robert Wapshott
 */
public class Tracer {
    private boolean tracing = false;

    public boolean isTracing() {
        return tracing;
    }

    public void setTracing(boolean tracing) {
        this.tracing = tracing;
    }

    public void info(String format, String... args) {
        info(MessageFormat.format(format, (Object[])args));
    }

    public void info(String message) {
        System.out.println(message);
    }

    public void warning(String message) {
        System.err.println(message);
    }

    public RuntimeException error(Exception e) {
        throw new IllegalStateException(e);
    }

    public RuntimeException error(String message) {
        throw new IllegalStateException(message);
    }

    public IllegalStateException error(String format, String... args) {
        return new IllegalStateException(MessageFormat.format(format, (Object[])args));
    }

    public void trace(String message) {
        System.out.println("Debug: " + message);
    }

    public void trace(String format, String... args) {
        trace(MessageFormat.format(format, (Object[])args));
    }

    public static String getExceptionText(Throwable e) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(out);
        e.printStackTrace(writer);
        writer.flush();
        writer.close();
        return new String(out.toByteArray());
    }
}
