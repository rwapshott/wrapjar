package uk.co.gencoreoperative.executable;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import uk.co.gencoreoperative.executable.jar.JarReader;
import uk.co.gencoreoperative.executable.jar.Repackager;
import uk.co.gencoreoperative.executable.script.ScriptWriter;
import uk.co.gencoreoperative.fileutils.FileUtils;
import uk.co.gencoreoperative.fileutils.StreamUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;

/**
 * The entry point for the application. This class is responsible for controlling the other elements
 * of this application to wrap a Jar inside a script.
 *
 * @author Robert Wapshott
 */
public class WrapJar {
    private List<String> paths = new LinkedList<String>();
    private JarReader reader;
    private Repackager repackager;
    private ScriptWriter writer;
    private Tracer tracer;
    private String mainClass = null;

    @Inject
    public WrapJar(JarReader reader, Repackager repackager, ScriptWriter writer, Tracer tracer) {
        this.reader = reader;
        this.repackager = repackager;
        this.writer = writer;
        this.tracer = tracer;
    }

    public void setPaths(List<String> paths) {
        this.paths.addAll(paths);
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public List<String> getPaths() {
        return paths;
    }

    public boolean process() {
        if (getPaths().isEmpty()) {
            help();
            return false;
        }

        // Find the main class in each Jar file provided.
        if (mainClass == null) {
            Collection<JarFile> jars = loadJars(getPaths());
            for (JarFile jarFile : jars) {

                tracer.info("Examining {0} for a Main Class", jarFile.getName());
                mainClass = reader.getMainClass(jarFile);
                if (mainClass == null) {
                    continue;
                }
                tracer.info("Found Main Class: {0}", jarFile.getName());
                break;
            }
        }

        if (mainClass == null) {
            throw new IllegalStateException("Could not find Main Class in the Jars provided.");
        }


        // Repackage all Jars into one jar.
        File temp = repackager.repackage(mainClass, loadJars(paths));

        // Get the name of the main class for the Script name
        int pos = mainClass.lastIndexOf(".");
        String scriptName = mainClass;
        if (pos != -1) {
            scriptName = mainClass.substring(pos + ".".length(), mainClass.length());
        }

        // Generate the script.
        FileOutputStream scriptStream;
        tracer.info("Writing script {0}", scriptName);

        try {
            scriptStream = new FileOutputStream(scriptName, false);
            writer.write(temp, scriptStream);
            scriptStream.flush();
            scriptStream.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Clean up temporary jar
        try {
            FileUtils.delete(temp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    private Collection<JarFile> loadJars(Collection<String> paths) {
        List<JarFile> r = new LinkedList<JarFile>();
        for (String path : paths) {
            try {
                r.add(new JarFile(path));
            } catch (IOException e) {
                throw tracer.error("Failed to open jar: {0}", path);
            }
        }
        return r;
    }

    /**
     * Provides help information from the HELP file.
     */
    private void help() {
        InputStream stream = WrapJar.class.getResourceAsStream("/HELP.txt");
        for (String line : StreamUtils.readLinesToList(stream)) {
            tracer.info(line);
        }
    }

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new ExecutableModule());
        WrapJar wrapJar = injector.getInstance(WrapJar.class);
        Tracer tracer = injector.getInstance(Tracer.class);

        List<String> paths = new ArrayList<String>();
        for (String path : args) {
            File file = new File(path);
            if (!file.exists()) {
                wrapJar.setMainClass(path);
            } else {
                paths.add(path);
            }
        }
        wrapJar.setPaths(paths);


        try {
            boolean result = wrapJar.process();

            int exitCode = 0;
            if (!result) {
                exitCode = -1;
            }
            System.exit(exitCode);
        } catch (Throwable e) {
            tracer.warning(e.getMessage());
            if (tracer.isTracing()) tracer.trace(Tracer.getExceptionText(e));
        }
    }
}
