package uk.co.gencoreoperative.executable.jar;

import uk.co.gencoreoperative.fileutils.JarBuilder;
import uk.co.gencoreoperative.fileutils.StreamUtils;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

/**
 * Parses a Jar file and finds a Manifest if there is one.
 *
 * @author Robert Wapshott
 */
public class JarReader {
    /**
     * Searches for the main class in the Jar files manifest.
     *
     *
     * @param jarFile The path to the Jar to be examined.
     *
     * @return Null if no manifest or WrapJar-Class: found. Otherwise the name of the main class.
     *
     * @throws IOException If there was an unexpected error.
     */
    public String getMainClass(JarFile jarFile) {
        List<String> lines = new LinkedList<String>();
        try {
            ZipEntry entry = jarFile.getEntry(JarBuilder.MANIFEST);
            if (entry == null) {
                return null;
            }

            lines.addAll(StreamUtils.readLinesToList(jarFile.getInputStream(entry)));

            jarFile.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return parseManifest(lines);
    }

    private String parseManifest(List<String> contents) {
        for (String line : contents) {
            if (line.contains("Main-Class")) {
                String sep = ":";
                int pos = line.indexOf(sep);
                if (pos < 0) {
                    return null;
                }
                return line.substring(pos + sep.length()).trim();
            }
        }
        return null;
    }
}
