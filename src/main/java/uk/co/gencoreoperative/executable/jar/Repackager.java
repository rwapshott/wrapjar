package uk.co.gencoreoperative.executable.jar;

import com.google.inject.Inject;
import org.apache.commons.lang.RandomStringUtils;
import uk.co.gencoreoperative.executable.Tracer;
import uk.co.gencoreoperative.fileutils.JarBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Will take a number of Jar files and produce a single Jar from them.
 *
 * @author Robert Wapshott
 */
public class Repackager {
    private Tracer tracer;

    @Inject
    public Repackager(Tracer tracer) {
        this.tracer = tracer;
    }

    public File repackage(String className, Collection<JarFile> jars) {

        String name = RandomStringUtils.randomAlphabetic(10) + JarBuilder.JAR_EXTENSION;
        tracer.info("Target Jar: {0}", name);

        JarBuilder builder = JarBuilder.createJar(name).withCompression();
        // Repackage each Jar
        for (JarFile jar : jars) {
            tracer.info("Repackaging... {0}", jar.getName());
            repackageJar(jar, builder);
        }

        // Package the manifest
        tracer.info("Writing manifest...");
        String manifest = JarBuilder.createManifest().withMainClass(className).build();
        builder.withManifest(manifest);

        return builder.build();
    }

    /**
     * Note: This function will filter out all META-INF entries.
     *
     * @param jar The contents of this jar will be repackaged.
     * @param builder The target Jar to contain the contents.
     */
    private void repackageJar(JarFile jar, JarBuilder builder) {
        Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            String name = entry.getName();

            if (name.startsWith(JarBuilder.META_INF)) continue;

            if (entry.isDirectory()) {
                builder.addDirectory(name);
            } else {
                try {
                    builder.addFile(name, jar.getInputStream(entry));
                } catch (IOException e) {
                    throw tracer.error(e);
                }
            }
        }
    }
}
